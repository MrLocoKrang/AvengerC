﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    public float speed;
    // Use this for initialization

    // sprite control
    public Sprite spaceshipSprites_0; // Drag your first sprite here
    public Sprite spaceshipSprites_3; // Drag your second sprite here

    private SpriteRenderer spriteRenderer;

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>(); // we are accessing the SpriteRenderer that is attached to the Gameobject
        if (spriteRenderer.sprite == null) // if the sprite on spriteRenderer is null then
            spriteRenderer.sprite = spaceshipSprites_0; // set the sprite to sprite1
    }

    // Update is called once per frame
    void Update()
    {
        float x = Input.GetAxisRaw("Horizontal"); // value will be -1, 0 and 1 (for left, no input, and right)
        float y = Input.GetAxisRaw("Vertical"); // value will be -1, 0 and 1 (for down, no input, and up)

        // now based on the input we compute a direction vector, and we normalize it to get a unit vector
        Vector2 direction = new Vector2(x, y).normalized;

        //now we call the function that computes and sets the players position
        Move(direction);
    
    
        if (Input.GetKeyDown(KeyCode.UpArrow)) // If the up arrow is pushed down
        {
            ChangeTheDamnSprite(); // call method to change sprite
}
    }

    void ChangeTheDamnSprite()
{
    if (spriteRenderer.sprite == spaceshipSprites_0) // if the spriteRenderer sprite = sprite1 then change to sprite2
    {
        spriteRenderer.sprite = spaceshipSprites_3;
    }
    else
    {
        spriteRenderer.sprite = spaceshipSprites_0; // otherwise change it back to sprite1
    }
}
void Move(Vector2 direction)
    {
        // find the screen limits to the players movement (left, right, top, and bottom edges of the screen)
        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0)); // this is the bottom left point of the screen
        Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1)); // this is the top right point of the screen

        max.x = max.x - 0.355f; // substract the player sprite half width
        min.x = min.x + 0.355f; // add the player sprite half width

        max.y = max.y - 0.43f; // substract the player sprite half height
        min.y = min.y + 0.43f; // add the player sprite half height

        // get the player current position
        Vector2 pos = transform.position;

        // Calculate the new position
        pos += direction * speed * Time.deltaTime;

        // Make sure the new position isnt outside the screen
        pos.x = Mathf.Clamp(pos.x, min.x, max.x);
        pos.y = Mathf.Clamp(pos.y, min.y, max.y);

        // Update the players current position
        transform.position = pos;
    }
}